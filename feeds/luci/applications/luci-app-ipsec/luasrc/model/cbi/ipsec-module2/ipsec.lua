-- CBI file: describe the structure of an UCI config file and the
-- resulting HTML from to be evaluated by the CBI parser
-- Licensed to the public under the Apache License 2.0.
-- Low-level and high-level filesystem manipulation library.
------------------------
local fs  = require "nixio.fs"
local sys = require "luci.sys"
local uci = require "luci.model.uci".cursor()
local uci1 = require "luci.model.uci".Cursor
local testfullps = sys.exec("ps --help 2>&1 | grep BusyBox") --check which ps do we have
local psstring = (string.len(testfullps)>0) and  "ps w" or  "ps axfw" --set command we use to get pid
local mp = Map("ipsec", translate("IPSec Client-to-Server")) 

local st  = mp:section(SimpleSection)
st.template = "ipsec-module2/modeswitch"
st.mode = "client2server"
local s = mp:section(TypedSection,"client2server",translate("IPSec Connections"),translate("Below is a list of configured IPsecVPN connection and their current state"))

s.anonymous = true
s.template = "cbi/tblsection"
s.template_addremove = "ipsec-module/cbi-select-input-add"
s.addremove = true
s.add_select_options = { }
-------------------------------------------------------------
local cfg = s:option(DummyValue, "config")

function cfg.cfgvalue(self, section)
	local file_cfg = self.map:get(section, "config")
	if file_cfg then
		s.extedit = luci.dispatcher.build_url("admin", "vpn", "ipsecvpn2","file","%s")
	else
		s.extedit = luci.dispatcher.build_url("admin", "vpn", "ipsecvpn2", "advanced","%s")
	end
end 

uci:load("ipsec")
uci:foreach( "ipsec", "client2server",
	function(section)
		s.add_select_options[section['.name']] =
			section['_description'] or section['.name']
	end
)
--Grabs PID number- process id 
function s.getPID(section) -- Universal function which returns valid pid # or nil
	local pid = sys.exec("%s | grep -w '%s'" % { psstring, section })
	if pid and #pid > 0 then
		return tonumber(pid:match("^%s*(%d+)"))
		--return nil
	else
		return nil
    end
end
---------------------------------------------------------------------------
function s.parse(self, section)
	local recipe = luci.http.formvalue(
		luci.cbi.CREATE_PREFIX .. self.config .. "." ..
		self.sectiontype .. ".select"
	)
	if recipe and not s.add_select_options[recipe] then
		self.invalid_cts = true
	else
		TypedSection.parse( self, section )
	end
end
--------------------------------------------------------------------------------
function s.create(self, name)
	local name = luci.http.formvalue(
		luci.cbi.CREATE_PREFIX .. self.config .. "." ..
		self.sectiontype .. ".text"
	)
	if #name > 3 and not name:match("[^a-zA-Z0-9_]") then
		local s = uci:section("ipsec", "client2server", name)
		if s then
			uci:set("ipsec",name,"enabled","1")
			uci:set("ipsec",name,"authentication_method","psk")
			uci:set("ipsec",name,"pre_shared_key","123456789")
			uci:set("ipsec",name,"keyingtries","0")
			uci:set("ipsec",name,"ikelifetime","1h")
			uci:set("ipsec",name,"lifetime","8h")
			uci:set("ipsec",name,"dpddelay","10s")
			uci:set("ipsec",name,"dpdaction","restart")
			uci:set("ipsec",name,"dpdtimeout","20s")
			uci:set("ipsec",name,"remotegateway","0.0.0.0")
			uci:set("ipsec",name,"localgateway","0.0.0.0")
			uci:set_list("ipsec",name,"p1_proposal","pre_g2_aes_sha1")
			uci:set_list("ipsec",name,"tunnel","71_to_101")
			uci:save("ipsec")
			uci:commit("ipsec")
		end
	elseif #name > 0 then
		self.invalid_cts = true
	end
	return 0
end
-- ----------------------------------------------------------------------------
s.redirect = luci.dispatcher.build_url(
	"admin"
)
function s.remove(self,section)
	local leftcert = uci:get("ipsec",translate(section),"leftcert")
	local leftkey = uci:get("ipsec",translate(section),"leftkey")
	sys.call("rm '%s'-key.pem" % section)
	sys.call("rm '%s'-ca.pem" % section)
	sys.call("rm -r /www/'%s'" % section)
	sys.call("rm /etc/ipsec.d/certs/'%s'" % leftcert)
	sys.call("rm /etc/ipsec.d/private/'%s'" % leftkey)
	uci:delete("ipsec", section)
	uci:save("ipsec")
	uci:commit("ipsec")
	return 0
end

--------------------------------------------------------------------------------
local Named =s:option(DummyValue,"_name",translate("Name Connection"))
function Named.cfgvalue(self,section)
    return translate(section)
end
---------------------------------------------------------------------
s:option( Flag, "enabled", translate("Enabled") )

local active = s:option( DummyValue, "_active", translate("Started") )
function active.cfgvalue(self, section)
	local ret1 = sys.exec("ipsec status | grep %s" % section)
	if ret1 ~= "" then
		return translate("yes")
	else
		return translate("no")
	end
end
local download = s:option( Button, "_download", translate("Download") )
function download.write(self, section)
	sys.call("mkdir /www/'%s'" % section)
	sys.call("ln -s /www/'%s'-ca.pem /www/'%s'/" % {section,section})
    luci.http.redirect( '/%s' % section )

end
local updown = s:option( Button, "_updown", translate("Generated/Generate") )
updown._state = false
-- updown.redirect = luci.dispatcher.build_url(
-- 	"admin", "vpn", "ipsecvpn"
-- )
local url={'admin/vpn/ipsecvpn2'}
updown.redirect = url
local clock = os.clock
function updown.sleep(self , n)  -- seconds
  local t0 = clock()
  while clock() - t0 <= n do end
end
function updown.cbid(self, section)
	local ret = sys.exec("ls /www | grep '%s'" % section)
	if ret ~= "" then
		self._state = true
	else
		self._state = false
	end
	self.option = self._state and "generated" or "generate"
	return AbstractValue.cbid(self, section)
end
function updown.cfgvalue(self, section)
	self.title = self._state and "Generated" or "Generate"
	self.inputstyle = self._state and "reset" or "reload"
end
function updown.write(self, section, value)
	local leftcert = uci:get("ipsec",translate(section),"leftcert")
	local leftkey = uci:get("ipsec",translate(section),"leftkey")
	local local_identifier = uci:get("ipsec",translate(section),"local_identifier")
	local rightsourceip = uci:get("ipsec",translate(section),"rightsourceip")
	if self.option == "generated" then
		--sys.call("/etc/init.d/ipsec stop %s" % section)
		--sys.call("ipsec down esp")
		--self.sleep(10)
		--sys.call("/etc/init.d/ipsec stop %s" % section)
		--print(sys.exec("ipsec down %s" % section))
	else
		sys.call("iptables -t nat -I POSTROUTING -m policy --pol ipsec --dir out -j ACCEPT")
		-- sys.call("iptables -t nat -A POSTROUTING -s 192.168.101.1/24 -d 192.168.100.1/24 -j MASQUERADE")
		-- localsubnet vs remotesubnet
		sys.call("ipsec pki --gen --type rsa --outform pem > '%s'-key.pem" % section)
		sys.call("ipsec pki --self --ca --lifetime 3650 --in '%s'-key.pem --type rsa --dn \"C=US, O=USAMI, CN=REM\" --outform pem > '%s'-ca.pem" % {section,section})
		sys.call("ipsec pki --gen --type rsa --outform pem > %s" % leftkey)
		sys.call("ipsec pki --pub --in '%s' --type rsa | ipsec pki --issue --lifetime 1825 --cacert '%s'-ca.pem --cakey '%s'-key.pem --dn \"C=US, O=USAMI, CN='%s'\" --san '%s' --flag serverAuth --flag ikeIntermediate --outform pem > '%s'" % {leftkey,section,section,local_identifier,local_identifier,leftcert})
		sys.call("mv ./%s /etc/ipsec.d/certs/%s" % {leftcert,leftcert})
		sys.call("mv ./%s /etc/ipsec.d/private/%s" % {leftkey,leftkey})
		sys.call("iptables -A FORWARD --match policy --pol ipsec --dir in  --proto esp -s %s -j ACCEPT" % rightsourceip)
		sys.call("iptables -A FORWARD --match policy --pol ipsec --dir out  --proto esp -s %s -j ACCEPT" % rightsourceip)
		sys.call("iptables -t nat -A POSTROUTING -s %s -o eth0 -m policy --pol ipsec --dir out -j ACCEPT" % rightsourceip)
		sys.call("iptables -t nat -A POSTROUTING -s %s -o eth0 -j MASQUERADE" %rightsourceip)
		sys.call("iptables -A INPUT -p udp --dport  500 -j ACCEPT")
		sys.call("iptables -A INPUT -p udp --dport 4500 -j ACCEPT")
		sys.call("/etc/init.d/ipsec reload")
	end
	luci.http.redirect( '/cgi-bin/luci/admin/vpn/ipsecvpn2' )
end

function mp.on_after_commit(self,map)
	sys.call('/etc/init.d/ipsec reload')
end

function mp.on_after_apply(self,map)
	sys.call('/etc/init.d/ipsec restart')
end

return mp

