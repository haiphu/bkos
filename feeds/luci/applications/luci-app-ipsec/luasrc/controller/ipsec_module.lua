-- This source code is written for using IPsec VPN
-- Author : Duy Ho <duyhungho.work@gmail.com>
-- Date :7/11/2019
-- Licensed to the public under the Apache License 2.0.
-- Version 2
module("luci.controller.ipsec_module", package.seeall)

function index()
    --entry({"admin", "vpn"}, firstchild(),"IPSec VPN",45).dependent=false
    entry( {"admin", "vpn", "ipsecvpn"}, cbi("ipsec-module/ipsec"),_("Strongswan"))
    entry( {"admin", "vpn", "ipsecvpn","advanced"}, cbi("ipsec-module/ipsec-advanced"),nil).leaf = true
    entry( {"admin", "vpn", "ipsecvpn2"}, cbi("ipsec-module2/ipsec"),nil)
	entry( {"admin", "vpn", "ipsecvpn2","advanced"}, cbi("ipsec-module2/ipsec-advanced"),nil).leaf = true
end